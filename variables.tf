variable "region" {
  type        = string
  default     = "us-east-1"
  description = "(Optional; Default: us-east-1) AWS region where to create resources."
}

variable "bucket_name" {
  type        = string
  description = "Name for the bucket. This value will be lowercased and underscores replaced with hyphens. Also the final bucket name will have a random number appended to it."
}

variable "force_destroy" {
  type        = bool
  default     = "false"
  description = "(Optional, Default: false) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
}

variable "objects_expire" {
  type        = bool
  default     = true
  description = "(Optional, Default: true) Whether the objects in the bucket expire. This usually should not be changed from the default."
}

variable "versioning" {
  type = object({
    enabled    = bool
    mfa_delete = bool
  })
  default = {
    enabled    = false
    mfa_delete = false
  }
  description = "(Optional, Default: disabled) A state of versioning, as supported by the S3 bucket."
}

variable "expiration_days" {
  type        = number
  default     = 1460
  description = "(Optional, Default: 4y) The number of days for objects to be retained."
}

variable "expiration_days_noncurrent" {
  type        = number
  default     = 120
  description = "(Optional, Default: 120) The number of days for non-current objects to be retained."
}

variable "transition_days_standard_ia" {
  type        = number
  default     = 180
  description = <<EOF
    (Optional; Default: 180) Specifies the number of days (>=30) after object creation when the object transitions to STANDARD_IA storage.
    RECOMMENDATION: 30 days if objects meet minimum size.
    NOTE: Standard - IA has a minimum object size of 128KB. Smaller objects will be charged for 128KB of storage.
EOF
}

variable "transition_days_glacier" {
  type        = number
  default     = 540
  description = <<EOF
    (Optional; Default: 540) Specifies the number of days after object creation when the object transitions to GLACIER storage.
    RECOMMENDATION: 90 days, but there is approximately 40 KB of overhead per file so only use Glacier for larger files.
EOF
}

variable "admin_policy" {
  type = object({
    enabled     = bool
    iam_role_id = string
  })
  description = "Configures the IAM role for admin access to this bucket."
}

variable "read_only_policy" {
  type = object({
    enabled     = bool
    iam_role_id = string
  })
  default = {
    enabled     = false
    iam_role_id = ""
  }
  description = "(Optional; Default: disabled) Configures the IAM role for read-only access to this bucket."
}

variable "admin_policy_bucket_permissions" {
  type = list(string)
  default = [
    "s3:ListBucket",
    "s3:ListBucketVersions",
    "s3:ListBucketMultipartUploads",
  ]

  description = <<EOF
    (Optional) List of the permissions for bucket operations that you can specify for the admin policy.
    For reference: https://docs.aws.amazon.com/AmazonS3/latest/dev/using-with-s3-actions.html

    default = [
      "s3:ListBucket",
      "s3:ListBucketVersions",
      "s3:ListBucketMultipartUploads",
    ]
EOF
}

variable "admin_policy_object_permissions" {
  type = list(string)
  default = [
    "s3:AbortMultipartUpload",
    "s3:DeleteObject",
    "s3:DeleteObjectVersion",
    "s3:GetObject",
    "s3:GetObjectVersion",
    "s3:ListMultipartUploadParts",
    "s3:PutObject",
  ]

  description = <<EOF
    (Optional) List of the permissions for object operations that you can specify for the admin policy.
    For reference: https://docs.aws.amazon.com/AmazonS3/latest/dev/using-with-s3-actions.html

    default = [
      "s3:AbortMultipartUpload",
      "s3:DeleteObject",
      "s3:DeleteObjectVersion",
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:ListMultipartUploadParts",
      "s3:PutObject",
    ]
EOF
}

variable "public_access" {
  type = object({
    block_public_acls       = bool
    block_public_policy     = bool
    ignore_public_acls      = bool
    restrict_public_buckets = bool
  })
  default = {
    block_public_acls       = true
    block_public_policy     = true
    ignore_public_acls      = true
    restrict_public_buckets = true
  }
  description = <<EOF
    Manages S3 bucket-level Public Access Block configuration.
    For reference: https://docs.aws.amazon.com/AmazonS3/latest/dev/access-control-block-public-access.html
  EOF
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
