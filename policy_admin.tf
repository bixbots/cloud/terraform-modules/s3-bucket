data "aws_iam_policy_document" "admin_policy" {
  statement {
    effect    = "Allow"
    actions   = var.admin_policy_bucket_permissions
    resources = [aws_s3_bucket.primary.arn]
  }

  statement {
    effect    = "Allow"
    actions   = var.admin_policy_object_permissions
    resources = ["${aws_s3_bucket.primary.arn}/*"]
  }
}

resource "aws_iam_policy" "admin_policy" {
  count = var.admin_policy.enabled ? 1 : 0

  name        = "${local.bucket_name}-admin"
  description = "Grants the access to the ${local.bucket_name} S3 bucket for admin access"
  policy      = data.aws_iam_policy_document.admin_policy.json
}

resource "aws_iam_role_policy_attachment" "admin_policy" {
  count = var.admin_policy.enabled ? 1 : 0

  policy_arn = aws_iam_policy.admin_policy[count.index].arn
  role       = var.admin_policy.iam_role_id
}
