locals {
  read_only_policy_bucket_permissions = [
    "s3:ListBucket",
    "s3:ListBucketVersions",
    "s3:ListBucketMultipartUploads",
  ]

  read_only_policy_object_permissions = [
    "s3:GetObject",
    "s3:GetObjectVersion",
    "s3:ListMultipartUploadParts",
  ]
}

data "aws_iam_policy_document" "read_only_policy" {
  statement {
    effect    = "Allow"
    actions   = local.read_only_policy_bucket_permissions
    resources = [aws_s3_bucket.primary.arn]
  }

  statement {
    effect    = "Allow"
    actions   = local.read_only_policy_object_permissions
    resources = ["${aws_s3_bucket.primary.arn}/*"]
  }
}

resource "aws_iam_policy" "read_only_policy" {
  count = var.read_only_policy.enabled ? 1 : 0

  name        = "${local.bucket_name}-read-only"
  description = "Grants the access to the ${local.bucket_name} S3 bucket for read-only access"
  policy      = data.aws_iam_policy_document.read_only_policy.json
}

resource "aws_iam_role_policy_attachment" "read_only_policy" {
  count = var.read_only_policy.enabled ? 1 : 0

  policy_arn = aws_iam_policy.read_only_policy[count.index].arn
  role       = var.read_only_policy.iam_role_id
}
