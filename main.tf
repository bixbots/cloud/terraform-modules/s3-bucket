locals {
  bucket_name = replace(lower(var.bucket_name), "_", "-")

  min_ia_days      = 30
  min_glacier_days = 90
}

resource "aws_s3_bucket" "primary" {
  bucket_prefix = "${local.bucket_name}-"
  force_destroy = var.force_destroy

  tags = merge(var.tags, {
    "Name" = local.bucket_name,
  })
}

resource "aws_s3_bucket_acl" "primary" {
  bucket = aws_s3_bucket.primary.id

  acl = "private"
}

resource "aws_s3_bucket_versioning" "primary" {
  bucket = aws_s3_bucket.primary.id

  versioning_configuration {
    status     = var.versioning.enabled ? "Enabled" : "Disabled"
    mfa_delete = var.versioning.enabled ? (var.versioning.mfa_delete ? "Enabled" : "Disabled") : null
  }
}

resource "aws_s3_bucket_public_access_block" "primary" {
  bucket = aws_s3_bucket.primary.id

  block_public_acls       = var.public_access.block_public_acls
  block_public_policy     = var.public_access.block_public_policy
  ignore_public_acls      = var.public_access.ignore_public_acls
  restrict_public_buckets = var.public_access.restrict_public_buckets
}

resource "aws_s3_bucket_server_side_encryption_configuration" "primary" {
  bucket = aws_s3_bucket.primary.id

  rule {
    bucket_key_enabled = true
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "primary" {
  bucket = aws_s3_bucket.primary.id

  rule {
    id     = "general"
    status = "Enabled"

    abort_incomplete_multipart_upload {
      days_after_initiation = 7
    }
  }

  rule {
    id     = "expiration"
    status = var.objects_expire ? "Enabled" : "Disabled"

    expiration {
      days = var.expiration_days
    }

    noncurrent_version_expiration {
      noncurrent_days = var.expiration_days_noncurrent
    }
  }

  rule {
    id     = "transition_standard_ia"
    status = (var.expiration_days - var.transition_days_standard_ia) > local.min_ia_days ? "Enabled" : "Disabled"

    transition {
      days          = var.transition_days_standard_ia
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      noncurrent_days = (var.expiration_days_noncurrent - var.transition_days_standard_ia > local.min_ia_days) ? local.min_ia_days : (var.expiration_days_noncurrent + local.min_ia_days)
      storage_class   = "STANDARD_IA"

      # If the non-current version of an object is not going to live in s3-ia for at least 30 days,
      # this sets the transition to ia to be 30 days after the object is set to expire to prevent the object from ever moving to ia.
    }
  }

  rule {
    id     = "transition_glacier"
    status = (var.expiration_days - var.transition_days_glacier) > local.min_glacier_days ? "Enabled" : "Disabled"

    transition {
      days          = var.transition_days_glacier
      storage_class = "GLACIER"
    }

    noncurrent_version_transition {
      noncurrent_days = (var.expiration_days_noncurrent - var.transition_days_glacier > local.min_glacier_days) ? local.min_glacier_days : (var.expiration_days_noncurrent + local.min_glacier_days)
      storage_class   = "GLACIER"

      # If the non-current version of an object is not going to live in glacier for at least 90 days,
      # this sets the transition to glacier to be 90 days after the object is set to expire to prevent the object from ever moving to glacier.
    }
  }
}
