# s3-bucket

## Summary

Creates an S3 bucket. Buckets are set to be server-side-encrypted with AES256 by default. Transitioning to S3 IA (Infrequent Access) and Glacier tiers can be easily configured. By default, object versioning is disabled.

## Resources

This module creates the following resources:

* [aws_s3_bucket](https://www.terraform.io/docs/providers/aws/r/s3_bucket.html)
* [aws_s3_bucket_public_access_block](https://www.terraform.io/docs/providers/aws/r/s3_bucket_public_access_block.html)
* [aws_s3_bucket_policy](https://www.terraform.io/docs/providers/aws/r/s3_bucket_policy.html)
* [aws_iam_policy](https://www.terraform.io/docs/providers/aws/r/iam_policy.html)
* [aws_iam_role_policy_attachment](https://www.terraform.io/docs/providers/aws/r/iam_role_policy_attachment.html)

## Cost

Use of this module will incur fees depending on the storage consumed, access requests, and your lifecycle needs. Before using this module, please familiarize yourself with the expenses associated with S3.

* AWS S3 pricing information is available [here](https://aws.amazon.com/s3/pricing/)

## Inputs

| Name                            | Description                                                                                                                                                                                                                                                                       | Type           | Default                                   | Required |
|:--------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------|:------------------------------------------|:---------|
| region                          | AWS region where to create resources.                                                                                                                                                                                                                                             | `string`       | `us-east-1`                               | yes      |
| bucket_name                     | Name for the bucket. This value will be lowercased and underscores replaced with hyphens. Also the final bucket name will have a random number appended to it.                                                                                                                    | `string`       | -                                         | yes      |
| force_destroy                   | A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable.                                                                                                                      | `bool`         | `false`                                   | no       |
| objects_expire                  | Whether the objects in the bucket expire. This usually should not be changed from the default.                                                                                                                                                                                    | `bool`         | `true`                                    | no       |
| versioning                      | A state of versioning, as supported by the S3 bucket.                                                                                                                                                                                                                             | `object`       | disabled                                  | no       |
| expiration_days                 | The number of days for objects to be retained.                                                                                                                                                                                                                                    | `number`       | `1460` (4 years)                          | no       |
| expiration_days_noncurrent      | The number of days for non-current objects to be retained.                                                                                                                                                                                                                        | `number`       | `120`                                     | no       |
| transition_days_standard_ia     | Specifies the number of days (>=30) after object creation when the object transitions to STANDARD_IA storage. RECOMMENDATION: 30 days if objects meet minimum size. NOTE: Standard - IA has a minimum object size of 128KB. Smaller objects will be charged for 128KB of storage. | `number`       | 180                                       | no       |
| transition_days_glacier         | Specifies the number of days after object creation when the object transitions to GLACIER storage. RECOMMENDATION: 90 days, but there is approximately 40 KB of overhead per file so only use Glacier for larger files.                                                           | `number`       | 540                                       | no       |
| admin_policy                    | Configures the IAM role for admin access to this bucket.                                                                                                                                                                                                                          | `object`       | -                                         | yes      |
| read_only_policy                | Configures the IAM role for read-only access to this bucket.                                                                                                                                                                                                                      | `object`       | disabled                                  | no       |
| admin_policy_bucket_permissions | List of the permissions for bucket operations that you can specify for the admin policy.                                                                                                                                                                                          | `list(string)` | _(see source)_                            | no       |
| admin_policy_object_permissions | List of the permissions for object operations that you can specify for the admin policy.                                                                                                                                                                                          | `list(string)` | _(see source)_                            | no       |
| public_access                   | Manages S3 bucket-level Public Access Block configuration.                                                                                                                                                                                                                        | `object`       | _(see source, all public access blocked)_ | no       |
| tags                            | Additional tags to attach to all resources created by this module.                                                                                                                                                                                                                | `map(string)`  | -                                         | no       |

## Outputs

| Name               | Description                                                                 |
|:-------------------|:----------------------------------------------------------------------------|
| bucket_id          | The name of the bucket.                                                     |
| bucket_arn         | The ARN of the bucket. Will be formated as `arn:aws:s3:::{bucket_id}`.      |
| bucket_domain_name | The bucket domain name. Will be formated as `{bucket_id}.s3.amazonaws.com`. |

## Examples

### Simple Usage

```terraform
module "s3_bucket" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/s3-bucket.git?ref=v1"

  bucket_name   = "example-bucket"
  admin_policy  = {
    enabled     = false
    iam_role_id = ""
  }
}
```

## Version History

* v1 - Initial Release
