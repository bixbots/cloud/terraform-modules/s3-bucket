data "aws_iam_policy_document" "bucket_policy" {
  statement {
    sid    = "RequireEncryptedTransport"
    effect = "Deny"

    actions = [
      "s3:*",
    ]

    resources = [
      "${aws_s3_bucket.primary.arn}/*",
    ]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values = [
        false,
      ]
    }

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.primary.id
  policy = data.aws_iam_policy_document.bucket_policy.json

  # This is to prevent the following error
  # OperationAborted: A conflicting conditional operation is currently in progress against this resource. Please try again.
  depends_on = [
    aws_s3_bucket_public_access_block.primary
  ]
}
